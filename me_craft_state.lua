-- enum table with crafting statuses
local MECraftProcessState = {
	never_started = 1, -- crafting was not requested yet (bad, should gc!)
	failed = 2, -- crafting failed to start (missing resources or craft processors)
	canceled = 3, -- crafting was started but then canceled
	running = 4,
	complete = 5,
}


local names = { }
names[MECraftProcessState.never_started] = "Accepted"
names[MECraftProcessState.failed] = "Failed to start"
names[MECraftProcessState.canceled] = "Canceled"
names[MECraftProcessState.running] = "In progress"
names[MECraftProcessState.complete] = "Completed"

MECraftProcessState.names = names

function MECraftProcessState.get_name(state)
	return MECraftProcessState.names[state] or "Unknown"
end

return MECraftProcessState