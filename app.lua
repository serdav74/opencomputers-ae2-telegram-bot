local Bot = require("bot")
local component = require("component")
local MEManager = require("me")
local MECraftProcessStage = require("me_craft_state")

MyBot = Bot:new()

function MyBot:init()
	self.me_manager = MEManager:new()
end

function MyBot:process_updates(updates)
	for index, update in ipairs(updates) do
		if update.message ~= nil then
			self:process_message(update.message)
		elseif update.edited_message ~= nil then
			print("Received update of class \"edited_message\", discarding.")
		else
			print("Received unsupported update, discarding.")
		end
	end
end

function MyBot:process_message(message)
	local text = message.text
	
	if string.find(text, "^/test") ~= nil then
		self:sendMessage(message.chat.id, "Your username is @" .. message.from.username)
	elseif string.find(text, "^/ae2") ~= nil then
		self:process_ae2_commands(message)
	end
end

function MyBot:process_ae2_commands(message)
	local text = message.text
	
	if string.find(text, "^/ae2 list") ~= nil then
		local items = self.me_manager:list_items()
		local result = ""
		for k, stack in ipairs(items) do
			result = result .. stack.label .. ": " .. stack.size .. "\n"
		end
		self:sendMessage(message.chat.id, result)
	elseif string.find(text, "^/ae2 craft") ~= nil then
		local l, r = string.find(text, "%d+", 10)
		local amount = tonumber(string.sub(text, l, r))
		local pattern = string.sub(text, r+2, -1)
		
		local success, results = self.me_manager:try_order_craft(pattern, amount)
		if success then
			local stack = results.getItemStack()
			self:sendMessage(message.chat.id, "<b>Accepted</b>\n" .. amount .. "x " .. stack.label)
		else
			if results == nil then
				self:sendMessage(message.chat.id, "<b>Rejected</b>\nNo craftables found.\n\nYou requested: <pre>" .. amount .. "x " .. pattern .. "</pre>")
			else
				local text = "<b>Rejected</b>\nMultiple results found. Try more specific queries.\n\nYou requested: <pre>" .. amount .. "x " .. pattern .. "</pre>\n\n<b>Results</b>"
				for _, result in ipairs(results) do
					local stack = result.getItemStack()
					text = text .. "\n - " .. stack.size .. "x " .. stack.label;
				end
				self:sendMessage(message.chat.id, text)
			end
		end
	end
end

function MyBot:process_me_updates(chat_id)
	local updates = self.me_manager:get_craftable_updates()
	for _, update in ipairs(updates) do
		local stack = update.process:get_stack()
		print(stack.label, update.old, update.new)
		
		local old_state = MECraftProcessStage.get_name(update.old)
		local new_state = MECraftProcessStage.get_name(update.new)
		text = "<b>Status changed</b>\nCrafting <b>" .. stack.label .. "</b>: <i>" .. new_state .. '</i> (was <i>' .. old_state .. '</i>)'
		self:sendMessage(chat_id, text)
	end
end


function MyBot:loop()
	while (true) do
		self:process_me_updates(self.admin_chat)
		local updates = self:getUpdates()
		self:process_updates(updates)
		os.sleep(2)
	end
end

local admin_chat = 123456789

local bot = MyBot:new({ token = '123456789:example', admin_chat = admin_chat })
bot:init()
bot:sendMessage(admin_chat, "Reading updates...")
bot:loop()
