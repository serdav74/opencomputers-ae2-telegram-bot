local component = require("component")
local MECraftProcessState = require("me_craft_state")

-- instances of this class represent crafting request to a network, either successful or not
local MECraftProcess = {
	
}

function MECraftProcess:new(craftable)
	local obj = {
		craftable=craftable,
		status_proxy=nil,
		last_state=MECraftProcessState.never_started,
		failed=false
	}
	
	setmetatable(obj, self)
	self.__index = self
	
	return obj
end

-- sends a crafting requests and checks whether it started successfully
function MECraftProcess:request(amount)
	self.status_proxy = self.craftable.request(amount)
	local canceled, reason = self.status_proxy.isCanceled()
	if canceled then
		self.failed = true
	end
	return not failed
end

function MECraftProcess:get_stack()
	return self.craftable.getItemStack()
end

-- infers current state of the request
function MECraftProcess:get_state()
	if self.status_proxy == nil then
		return MECraftProcessState.never_started;
	elseif self.failed then
		return MECraftProcessState.failed;
	end
	local done = self.status_proxy.isDone()
	local canceled, reason = self.status_proxy.isCanceled()
	if canceled then
		print("Crafting process was canceled with reason: " .. reason)
		return MECraftProcessState.canceled;
	elseif done then
		return MECraftProcessState.complete;
	else
		return MECraftProcessState.running;
	end
end

-- checks whether state has changed since requesting or previous update check
function MECraftProcess:get_update()
	local current_state = self:get_state()
	local last_state = self.last_state
	
	if current_state ~= self.last_state then
		self.last_state = current_state
		return last_state, current_state
	else
		return last_state, nil
	end
end


local MEManager = {
	component_name = "me_controller"
}

function MEManager:new(obj)
	obj = obj or {}
	
	setmetatable(obj, self)
	self.__index = self
	
	obj.proxy = component.getPrimary(self.component_name)
	obj.processes = {}
	
	return obj
end

-- returns table of all items in network
-- TODO: add search feature
function MEManager:list_items()
	return self.proxy.getItemsInNetwork()
end

-- returns table of all recipes in network
function MEManager:list_craftables()
	return self.proxy.getCraftables()
end

-- returns all recipes where result name matches pattern
function MEManager:find_craftables(pattern)
	local craftables = self.proxy.getCraftables()
	local results = {}
	for k, recipe in ipairs(craftables) do
		local stack = recipe.getItemStack()
		if string.find(stack.label, pattern) ~= nil then
			table.insert(results, recipe)
		end
	end
	return results
end

-- tries to request crafting of recipe where result name matches pattern
-- if exactly one recipe found, request it
function MEManager:try_order_craft(pattern, amount)
	local query_results = self:find_craftables(pattern)
	local count = #query_results
	print("[try_order_craft] " .. count .. " results for " .. pattern)
	if count == 0 then
		return false, nil;
	elseif count > 1 then
		return false, query_results;
	else
		local craftable = table.remove(query_results)
		local process = MECraftProcess:new(craftable)
		local success = process:request(amount)
		table.insert(self.processes, process)
		return true, craftable
	end
end

-- returns all MECraftProcess objects that have updated status
function MEManager:get_craftable_updates()
	local updates = {}
	local deletes = {}
	for index, process in ipairs(self.processes) do
		local old, new = process:get_update()
		if new ~= nil then
			table.insert(updates, {old=old, new=new, process=process})
		end
		if new == MECraftProcessState.complete then
			table.insert(deletes, index)
		end
	end
	
	for _, index in ipairs(deletes) do
		table.remove(self.processes, index)
	end
	
	return updates
end


return MEManager
