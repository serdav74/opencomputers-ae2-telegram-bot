# OpenComputers AE2 Telegram bot

Simple Telegram bot suited to control Applied Energistics' ME Network operations, like automatic crafting.

## Installation

Grab a JSON library, drop it at `./json/init.lua`, run `./app.lua`. Fill your `token` and `admin_chat` beforehand.

## To-do
[ ] Filtered item search
