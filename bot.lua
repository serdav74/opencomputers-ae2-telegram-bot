local internet = require("internet")
local json = require("json")


Bot = {
	token = "example",
	_last_update = nil,
	last_update_file = "./last_update.txt",
	admin_chat = nil,
	me_proxy = nil
}

function Bot:new (o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	
	return o
end

function Bot:api_call(method, payload)
	local url = 'https://api.telegram.org/bot' .. (self.token) .. '/' .. method
	local handle = internet.request(url, payload)
	local result = ""
	for chunk in handle do result = result .. chunk end
	return json.decode(result)
end

function Bot:sendMessage(chat_id, text)
	local payload = {
		chat_id=chat_id,
		parse_mode="HTML",
		text=text,
	}
	return self:api_call('sendMessage', payload)
end

function Bot:get_last_update()
	if self._last_update == nil then
		local buffer = io.open(self.last_update_file)
		local data = buffer:read("*n")
		buffer:close()
		self._last_update = data
	end
	-- print("Last update id: ", self._last_update)
	return self._last_update
end

function Bot:set_last_update(value)
	self._last_update = value
	
	local buffer = io.open(self.last_update_file, "w")
	buffer:write(self._last_update)
	buffer:close(handle)
end

function Bot:getUpdates()
	local last_update = self:get_last_update()
	local payload = {
		offset=last_update + 1
	}
	local result = self:api_call('getUpdates', payload)
	
	local new_last_update = 0
	for k, update in ipairs(result.result) do
		new_last_update = math.max(new_last_update, update.update_id)
	end
	if new_last_update > 0 then
		self:set_last_update(new_last_update)
	end
	return result.result
end

return Bot
